/*

    1. Заполнить массив случайными целыми числами. Вывести массив на экран. Переупорядочить
       в этом массиве элементы следующим образом: сначала по не убыванию нечетные числа,
       потом нули, потом прочие числа по не возрастанию. Вывести массив на экран.

*/

import java.util.Arrays;
import java.util.Comparator;
import java.util.Random;

public class Task1 {


    /**
     * Функция создания массива случайных целых чисел заданной величины.
     *
     * @param randomNumberOrigin левая граница множества (включительно) из которого будут браться случайные числа.
     * @param randomNumberBound  правая граница множества (не включительно) из которого будут браться случайные числа.
     * @param size               размер массива, который нужно создать.
     * @return возвращает массив int[]
     */
    public static int[] getRandomArray(int size, int randomNumberOrigin, int randomNumberBound) {
        Random random = new Random();

        return random.ints(size, randomNumberOrigin, randomNumberBound).toArray();
    }

    /**
     * Функция обертка для функции getRandomArray, для создания массива случайных целых чисел из [0; 100) заданной величины.
     *
     * @param size размер массива, который нужно создать.
     * @return возвращает массив int[]
     */
    public static int[] getRandomArray(int size) {
        return getRandomArray(size, 0, 100);
    }

    /**
     * Функция, которая изменяет порядок элементов по следующему принципу: сначала по не убыванию нечетные числа,
     * потом нули, потом прочие числа по не возрастанию.
     *
     * @param array массив, в котором нужно изменить порядок элементов (массив изменится).
     */
    public static void reorderArray(int[] array) {
        int[] oddNumbers = Arrays.stream(array)
                .filter(x -> x != 0 & x % 2 != 0)
                .sorted()
                .toArray();
        int[] evenNumbers = Arrays.stream(array)
                .boxed()
                .filter(x -> x != 0 & x % 2 == 0)
                .sorted(Comparator.reverseOrder())
                .mapToInt(Integer::intValue)
                .toArray();
        int[] zeroNumbers = Arrays.stream(array)
                .filter(x -> x == 0)
                .toArray();

        System.arraycopy(oddNumbers, 0, array, 0, oddNumbers.length);
        System.arraycopy(zeroNumbers, 0, array, oddNumbers.length, zeroNumbers.length);
        System.arraycopy(evenNumbers, 0, array, oddNumbers.length + zeroNumbers.length, evenNumbers.length);
    }

    public static void printArray(int[] arr) {
        System.out.println(Arrays.toString(arr));
    }

    public static void main(String[] args) {
        int[] arr = getRandomArray(10);

        printArray(arr);
        reorderArray(arr);
        printArray(arr);
    }
}
