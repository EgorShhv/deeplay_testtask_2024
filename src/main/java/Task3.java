import java.util.Arrays;
import java.util.Random;

public class Task3 {

    /**
     * Функция подсчитывает вероятность победы двух игроков по их последовательностям и количестве бросков кубика.
     * Приблизительная вероятность вычисляется по методу Монте-Карло. Число генераций эксперимента начинается со 100 и
     * с каждой итерацией увеличивается в 10 раз до тех пор, пока не выполнится одно из следующих условий: 1. не будет
     * достигнуто максимальное число генераций эксперимента, 2. разница между вероятностями на двух итерациях не будет
     * превышать заданное малое eps.
     *
     * @param player1Sequence массив int[] - последовательность первого игрока.
     * @param player2Sequence массив int[] - последовательность второго игрока.
     * @param countThrows количество бросков кубиков игроками.
     * @param eps малое eps.
     * @param maxIterations максимальное число экспериментов.
     *
     * @return массив double[] элементами которого являются {вероятность_победы_игрока_1, вероятность_победы_игрока_2,
     * вероятность_ничьей}.
     * */
    public static double[] getProbabilities(int[] player1Sequence, int[] player2Sequence, int countThrows, double eps, long maxIterations) {
        Random random = new Random();

        // Переменные для запоминания вероятности.
        double victory1Probability = 0.0;
        double victory2Probability = 0.0;
        double drawProbability;

        // Переменные для запоминания вероятности на предыдущей итерации.
        double lastProbability1;
        double lastProbability2;

        int[] diceSequence = new int[countThrows];

        int numIterations = 10;

        do {
            numIterations *= 10;

            // Запоминаем последние вероятности победы.
            lastProbability1 = victory1Probability;
            lastProbability2 = victory2Probability;

            int player1Wins = 0;
            int player2Wins = 0;
            int draws = 0;


            // Проводим numIterations экспериментов.
            for (int i = 0; i < numIterations; i++) {
                diceSequence = Arrays.stream(diceSequence).map(elem -> random.nextInt(6) + 1).toArray();

                int player1Score = countMatch(player1Sequence, diceSequence);
                int player2Score = countMatch(player2Sequence, diceSequence);

                if (player1Score > player2Score) {
                    player1Wins++;
                } else if (player2Score > player1Score) {
                    player2Wins++;
                } else {
                    draws++;
                }
            }

            // Обновляем новые вероятности победы.
            victory1Probability = (double) player1Wins / numIterations;
            victory2Probability = (double) player2Wins / numIterations;
            drawProbability = (double) draws / numIterations;

        } while (numIterations < maxIterations
                && Math.abs(victory1Probability - lastProbability1) > eps
                && Math.abs(victory2Probability - lastProbability2) > eps
        );

        System.out.println("Вероятность победы игрока 1:\t" + victory1Probability);
        System.out.println("Вероятность победы игрока 2:\t" + victory2Probability);
        System.out.println("Вероятность ничьей:         \t" + drawProbability);

        return new double[]{victory1Probability, victory2Probability, drawProbability};
    }

    /**
     * Функция подсчета количества совпадений одной последовательности в другой.
     *
     * @param sequence     массив int[], количество вхождений которого нужно найти в другом.
     * @param diceSequence массив int[], в котором будут искаться вхождения первого.
     *
     * @return число вхождений первого массива во второй без пересечений.
     */
    public static int countMatch(int[] sequence, int[] diceSequence) {
        int score = 0;
        for (int j = 0; j < diceSequence.length - sequence.length + 1; j++) {
            if (Arrays.equals(sequence, Arrays.copyOfRange(diceSequence, j, j + sequence.length))) {
                score++;
                j += sequence.length - 1;
            }
        }
        return score;
    }

    public static void main(String[] args) {
        int[] player1Sequence = {4, 4, 4};
        int[] player2Sequence = {1, 2, 3};
        long maxThrows = 100000; // Количество раундов игры.
        int sequenceLength = 1000; // Количество бросков кубика.
        double eps = 1e-5;

        getProbabilities(player1Sequence, player2Sequence, sequenceLength, eps, maxThrows);
    }
}