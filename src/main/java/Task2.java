/*

    2. Найти в массиве наиболее часто встречающееся число (числа, если таких несколько),
    вывести на экран исходные данные и результаты.

*/


import java.util.*;

public class Task2 {

    /**
     * Функция для нахождения наиболее часто встречаемых чисел в массиве.
     * @param array массив, в котором нужно найти часто встречаемые числа.
     * @return массив int[], который держит одно или несколько самых встречаемых чисел (если их частота одинакова).
     * */
    public static int[] getFrequentNumbers(int[] array) {
        if (array.length == 0) return new int[]{};

        Map<Integer, Integer> frequencyMap = new HashMap<>();
        for (int num : array) {
            frequencyMap.put(num, frequencyMap.getOrDefault(num, 0) + 1);
        }

        int maxFrequency = 0;
        for (int frequency : frequencyMap.values()) {
            if (frequency > maxFrequency)
                maxFrequency = frequency;
        }

        List<Integer> result = new ArrayList<>();
        for (Map.Entry<Integer, Integer> entry : frequencyMap.entrySet()) {
            if (entry.getValue() == maxFrequency)
                result.add(entry.getKey());
        }

        return result.stream().mapToInt(i -> i).toArray();
    }

    public static void main(String[] args) {
        int[] array = new int[]{};

        System.out.println(Arrays.toString(array));
        System.out.println(Arrays.toString(getFrequentNumbers(array)));
    }
}
