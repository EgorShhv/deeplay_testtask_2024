import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class Task1Test {

    @Test
    void reorderArray_EMPTY_ARRAY() {
        int[] actual = new int[]{};

        Task1.reorderArray(actual);

        int[] expected = new int[]{};

        Assertions.assertArrayEquals(expected, actual);
    }

    @Test
    void reorderArray_ONLY_ODD_NUMBERS() {
        int[] actual = new int[]{3, 5, 1, 9, 7};

        Task1.reorderArray(actual);

        int[] expected = new int[]{1, 3, 5, 7, 9};

        Assertions.assertArrayEquals(expected, actual);
    }


    @Test
    void reorderArray_ONLY_EVEN_NUMBERS() {
        int[] actual = new int[]{4, 2, 8, 6, 10};

        Task1.reorderArray(actual);

        int[] expected = new int[]{10, 8, 6, 4, 2};

        Assertions.assertArrayEquals(expected, actual);
    }

    @Test
    void reorderArray_ONLY_ZERO_NUMBERS() {
        int[] actual = new int[]{0, 0, 0, 0};

        Task1.reorderArray(actual);

        int[] expected = new int[]{0, 0, 0, 0};

        Assertions.assertArrayEquals(expected, actual);
    }

    @Test
    void reorderArray_ODD_EVEN_ZERO_NUMBERS() {
        int[] actual = new int[]{0, 1, 2, 7, 4, 0, 1, 4, 8, 5};

        Task1.reorderArray(actual);

        int[] expected = new int[]{1, 1, 5, 7, 0, 0, 8, 4, 4, 2};

        Assertions.assertArrayEquals(expected, actual);
    }
}