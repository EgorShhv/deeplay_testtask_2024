import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Task3Test {

    @Test
    void countMatch_NO_MATCHES() {

        int[] firstSequence = new int[]{1, 2, 3};
        int[] secondSequence = new int[]{1, 1, 3, 4, 5, 6, 7, 8, 9, 10};


        int actual = Task3.countMatch(firstSequence, secondSequence);
        int expect = 0;

        Assertions.assertEquals(expect, actual);
    }

    @Test
    void countMatch_MATCHES_WITHOUT_OVERLAPS() {

        int[] firstSequence = new int[]{4, 2, 4};
        int[] secondSequence = new int[]{1, 4, 2, 4, 4, 4, 4, 4, 2, 4};


        int actual = Task3.countMatch(firstSequence, secondSequence);
        int expect = 2;

        Assertions.assertEquals(expect, actual);
    }

    @Test
    void countMatch_MATCHES_WITH_OVERLAPS() {

        int[] firstSequence = new int[]{4, 4, 4};
        int[] secondSequence = new int[]{1, 4, 2, 4, 4, 4, 4, 4, 4, 4};


        int actual = Task3.countMatch(firstSequence, secondSequence);
        int expect = 2;

        Assertions.assertEquals(expect, actual);
    }
}