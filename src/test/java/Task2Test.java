import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Task2Test {

    @Test
    void getFrequentNumbers_EMPTY_ARRAY() {
        int[] inputArray = new int[]{};

        int[] actual = Task2.getFrequentNumbers(inputArray);

        int[] expected = new int[]{};

        Assertions.assertArrayEquals(expected, actual);
    }

    @Test
    void getFrequentNumbers_ONE_ELEM() {
        int[] inputArray = new int[]{1};

        int[] actual = Task2.getFrequentNumbers(inputArray);

        int[] expected = new int[]{1};

        Assertions.assertArrayEquals(expected, actual);
    }

    @Test
    void getFrequentNumbers_ONE_FREQUENT_NUMBER() {
        int[] inputArray = new int[]{1, 2, 1, 3, 2, 1};

        int[] actual = Task2.getFrequentNumbers(inputArray);

        int[] expected = new int[]{1};

        Assertions.assertArrayEquals(expected, actual);
    }

    @Test
    void getFrequentNumbers_TWO_FREQUENT_NUMBER() {
        int[] inputArray = new int[]{1, 2, 1, 3, 2, 4};

        int[] actual = Task2.getFrequentNumbers(inputArray);

        int[] expected = new int[]{1, 2};

        Assertions.assertArrayEquals(expected, actual);
    }

    @Test
    void getFrequentNumbers_EQUAL_FREQUENT_NUMBER() {
        int[] inputArray = new int[]{1, 2, 3, 4, 5, 6};

        int[] actual = Task2.getFrequentNumbers(inputArray);

        int[] expected = new int[]{1, 2, 3, 4, 5, 6};

        Assertions.assertArrayEquals(expected, actual);
    }
}